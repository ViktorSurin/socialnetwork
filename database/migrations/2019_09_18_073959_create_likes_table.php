<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('content_id')->unsigned();
        });

        Schema::table('likes', function (Blueprint $table) {
            $table->unique(['type_id', 'content_id'] );
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('content_id', 'likes__content_id__posts__foreign')->references('id')->on('posts');
            $table->foreign('content_id', 'likes__content_id__comments__foreign')->references('id')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
